﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace exercise1
{
    class Program
    {

       static void SearchArray(string[] text)
        {
            Console.WriteLine("what are you searching for?");
            string temp = Console.ReadLine();
            int value = 0;
                for (int x = 0; x < text.Length; x++)
                {
                    if (text[x].Equals(temp) == true)
                    {
                        value = 1;
                        break;
                    }
                    else
                    {
                        value = 0;
                    }
                }
                if (value == 1)
                {
                     Console.WriteLine("your string was found within the array");
                }
                else
                {    Console.WriteLine("your string was not found"); 
                } 
        }

        static void Main(string[] args)
        {
            string[] text = { "abc", "def", "ghi", "jkl" };
            SearchArray(text);
            Console.ReadKey();
        }

        


    }
}
