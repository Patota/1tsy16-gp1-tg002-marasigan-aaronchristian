﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        public Warrior()
        {
            this.Type = "Warrior";
        }
        public override FightResult Fight(Card opponentCard)
        {
            if (this.Type == opponentCard.Type)
                return FightResult.Draw;
            else if (opponentCard.Type == "Mage")
                return FightResult.Lose;
            else
                return FightResult.Win;
        }
    }
}
