﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Assassin : Card
    {
        public Assassin()
        {
            this.Type = "Assassin";
        }
        public override FightResult Fight(Card opponentCard)
        {
            if (this.Type == opponentCard.Type)
                return FightResult.Draw;
            else if (opponentCard.Type == "Warrior")
                return FightResult.Lose;
            else
                return FightResult.Win;
        }
    }
}
