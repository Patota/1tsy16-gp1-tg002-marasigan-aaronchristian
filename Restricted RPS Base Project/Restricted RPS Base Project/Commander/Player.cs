﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>----------------------------------------------------
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            Console.WriteLine("1)Play, 2)Discard");
            int k = Convert.ToInt32(Console.Read());

            if (k == 1)
                PlayCard();
            else
                this.Discard();
        }

        /// <summary>----------------------------------------------------
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            this.DisplayCards();
            int k = Convert.ToInt32(Console.Read());

            return this.Cards[k];
        }
    }
}
