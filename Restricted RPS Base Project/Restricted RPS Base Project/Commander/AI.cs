﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            int k = RandomHelper.Range(0, 100);

            if (k >= 0 && k < 30)
                Discard();
            else
                PlayCard();
        }

        public override Card PlayCard()
        {
            int k = RandomHelper.Range(0, Cards.Count);

            return this.Cards[k];
        }
    }
}
