﻿using UnityEngine;
using System.Collections;

public class EnemyBulletMove : BaseClassBullet 
{
    GameObject player;
	Vector3 dir;
	basicBullet bullet = new basicBullet();

	// Use this for initialization
	void Start () 
    {
		player = GameObject.FindGameObjectWithTag ("player");
		dir = player.transform.position - transform.position;
		//transform.LookAt (player.transform.position);	
	}

	// Update is called once per frame
	void Update () 
    {
		bullet.bulletMove (dir, 2, gameObject);
	}
}
