﻿using UnityEngine;
using System.Collections;

public class EnemyCannon : BaseClassShip
{
    public Rigidbody bullet1;
    public EnemyShip defaultShip = new EnemyShip(1, 3, 1);

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("spawnBullet", Random.Range(1.0f, 7.0f), Random.Range(1.0f, 7.0f));
    }

    // Update is called once per frame
    void Update()
    {
    }

    void spawnBullet()
    {
        defaultShip.ShootCannon(bullet1, gameObject);
        //clone.transform.LookAt(player.transform.position);
        //clone.rigidbody.AddForce((player.transform.position - bullet.transform.position) * 4);
    }
}

