﻿using UnityEngine;
using System.Collections;

public class EnemyShipMove : BaseClassShip
{
    EnemyShip defaultship = new EnemyShip(3, 2, 1);
	GameObject player;
	Vector3 targetPos;
	
	// Use this for initialization
	void Start () 
    {
		player = GameObject.FindGameObjectWithTag("player");
	}

	// Update is called once per frame
	void Update () 
    {
		targetPos = player.transform.position - transform.position;

        //Debug.Log (Vector3.Distance (transform.position, player.transform.position));

        if (Vector2.Distance(transform.position, player.transform.position) > 6) 
        {
			defaultship.Move(gameObject, targetPos);
		}

		if (Vector2.Distance(transform.position, player.transform.position) < 2) 
        {
			defaultship.Move(gameObject, targetPos);
		}
	}
}
