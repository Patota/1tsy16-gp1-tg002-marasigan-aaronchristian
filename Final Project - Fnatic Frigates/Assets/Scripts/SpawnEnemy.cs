﻿using UnityEngine;
using System.Collections;

public class SpawnEnemy : MonoBehaviour 
{
	public Rigidbody enemy;
	public Rigidbody enemyBoss;
	public Rigidbody enemyShark;
	public GameObject player;
	public float speed;

	// Use this for initialization
	void Start () 
    {
		InvokeRepeating ("SpawnAIRandom", 1, Random.Range (1.0f, 3.0f));
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    void SpawnAIRandom()
    {
        int x, y;

        do
        {
            y = Random.Range(-1, 2);
            x = Random.Range(-1, 2);
        } while (x == 0 && y == 0);

        int enemyType = Random.Range(1, 11);

        if (player == true)
        {
            spawnAI(enemyType, x, y);
        }
	}

    void spawnAI(int type, int multiplierX, int multiplierY)
    {
        Rigidbody chosenType = enemy;

        if (type >= 1 && type < 5)
        {
            chosenType = enemy;
        }

        if (type >= 6 && type < 10)
        {
            chosenType = enemyShark;
        }

        if (type == 10)
        {
            chosenType = enemyBoss;
        }
        /*switch (type)
        {
            case 1: chosenType = enemy; break;
            case 2: chosenType = enemyBoss; break;
            case 3: chosenType = enemyShark; break;
            default: break;
        */
        chosenType = Instantiate(chosenType, new Vector3(player.transform.position.x + Random.Range(25, 30) * multiplierX, player.transform.position.y + Random.Range(25, 30) * multiplierY, 0.0f), transform.rotation) as Rigidbody;
    }
}
