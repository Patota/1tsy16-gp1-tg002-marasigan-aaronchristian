﻿using UnityEngine;
using System.Collections;

public class BaseClassShip : MonoBehaviour
{
    public class BasicShip
    {
        public int PlayerHealth;
        protected int MoveSpeed;
        public int FireRate;

        public void ShootCannon(Rigidbody bullet, GameObject thisObject)
        {
            Transform clone;
            clone = Instantiate(bullet, thisObject.transform.position, thisObject.transform.rotation) as Transform;
        }

        public void Move(GameObject thisShip, Vector3 targetPos)
        {
            thisShip.transform.position += 0.3f * targetPos * Time.deltaTime;
        }
    }

    public class PlayerShip : BasicShip
    {
        public PlayerShip(int hp, int spd, int fr)
        {
            MoveSpeed = spd;
            FireRate = fr;
            PlayerHealth = hp;
        }

        public PlayerShip()
        {
        }
    }

    public class EnemyShip : BasicShip
    {
        public EnemyShip(int hp, int spd, int fr)
        {
            MoveSpeed = spd;
            FireRate = fr;
            PlayerHealth = hp;
        }
    }

    public class EnemyShark : BasicShip
    {
        public EnemyShark(int hp, int spd)
        {
            MoveSpeed = spd;
            PlayerHealth = hp;
        }
    }

    public class EnemyBoss : BasicShip
    {
        public EnemyBoss(int hp, int spd, int fr)
        {
            MoveSpeed = spd;
            FireRate = fr;
            PlayerHealth = hp;
        }
    }
}