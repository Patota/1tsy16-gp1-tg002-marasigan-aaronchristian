﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : BaseClassShip
{
    public Slider PlayerHPSlider;
    public PlayerShip player = new PlayerShip(5, 4, 5);

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "bullet" || col.gameObject.tag == "enemyShark")
        {
            player.PlayerHealth -= 1;
            PlayerHPSlider.value -= 1;
            Destroy(col.gameObject);

            if (player.PlayerHealth < 1)
            {
                destroyAll();
                Destroy(gameObject);
            }
        }
    }

    public void destroyAll()
    {
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("enemyShip"))
        {
            Destroy(o);
        }
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("enemyShark"))
        {
            Destroy(o);
        }
    }
}
