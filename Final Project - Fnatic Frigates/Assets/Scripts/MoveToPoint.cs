﻿using UnityEngine;
using System.Collections;

public class MoveToPoint : MonoBehaviour
{
    public float camSpeed;

	// Use this for initialization
	void Start () 
    {
	}

	// Update is called once per frame
	void Update () 
    {
		Vector3 targetPos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 5));
		transform.position = Vector3.MoveTowards (transform.position, targetPos, camSpeed * Time.deltaTime);
    }
}
