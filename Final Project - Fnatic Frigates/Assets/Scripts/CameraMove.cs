﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour 
{
	public int Boundary = 200; 
	public float speed  = 0.01f;
	private int theScreenWidth;
	private int theScreenHeight;

    void Start()
    {
        theScreenWidth = Screen.width;
        theScreenHeight = Screen.height;
    }

    void Update()
    {
        if (Input.mousePosition.x > theScreenWidth - Boundary)
        {
            transform.position += new Vector3(speed * Time.deltaTime, 0.0f, 0.0f); // move on +X axis
        }

        if (Input.mousePosition.x < 0 + Boundary)
        {
            transform.position -= new Vector3(speed * Time.deltaTime, 0.0f, 0.0f); // move on -X axis
        }

        if (Input.mousePosition.y > theScreenHeight - Boundary)
        {
            transform.position += new Vector3(0.0f, speed * Time.deltaTime, 0.0f); // move on +Z axis
        } 

        if (Input.mousePosition.y < 0 + Boundary)
        {
            transform.position -= new Vector3(0.0f, speed * Time.deltaTime, 0.0f); // move on -Z axis
        }
	}
}
