﻿using UnityEngine;
using System.Collections;

public class BaseClassBullet : MonoBehaviour {

	public class basicBullet{

		public int bulletSpeed;
		public GameObject bulletTarget;
		public GameObject bulletPosition;

		public basicBullet(int spd,GameObject trgt,GameObject strtPos){
			bulletSpeed = spd;
			bulletTarget = trgt;
			bulletPosition = strtPos;
			
		}

		public basicBullet(){
		}


		public void bulletMove(){
			Vector3 dir = bulletTarget.transform.position - bulletPosition.transform.position;
			bulletPosition.transform.position += 3 * dir * Time.deltaTime;
				}

		public void bulletMove(Vector3 direction, int bulletSpeed,GameObject thisBullet){
			thisBullet.transform.position += bulletSpeed * direction * Time.deltaTime;
		}




	}

	public class playerBullet : basicBullet{

	
		public playerBullet(){

		}

		void playerBulletMove(){
		}
	
	
	
	}
}
