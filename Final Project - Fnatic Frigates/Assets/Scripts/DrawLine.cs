﻿using UnityEngine;
using System.Collections;

public class DrawLine : MonoBehaviour 
{
	public GameObject player;
	  
	void Start() 
    {
	}
	
	void Update() 
    {
		Vector3 targetPos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 5));
		LineRenderer lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.SetPosition(0, player.transform.position);
		lineRenderer.SetPosition(1, targetPos);
	}
}
