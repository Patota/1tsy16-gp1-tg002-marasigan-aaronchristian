﻿using UnityEngine;
using System.Collections;

public class DestroyBullet : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
    {
        Destroy(gameObject, 5);
    }
	
	// Update is called once per frame
	void Update () {
        if (GameObject.FindGameObjectWithTag("player") == null || GameObject.FindGameObjectWithTag("player") == false)
        {
            Destroy(gameObject);
        }
    }
}
