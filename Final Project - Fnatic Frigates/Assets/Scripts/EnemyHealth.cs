﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHealth : BaseClassShip 
{
    public Text scoreText;
    GameObject Player;
	int enemyHP = 2;
	// Use this for initialization
	void Start () 
    {
        Player = GameObject.FindGameObjectWithTag("player");
	}
	
	// Update is called once per frame
	void Update () 
    {
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "playerBullet")
        {
            enemyHP -= 1;

            Destroy(col.gameObject);

            if (enemyHP < 1 || Player == false)
            {
                Destroy(gameObject);
                Score.score += 50;
            }
        }
    }
}
