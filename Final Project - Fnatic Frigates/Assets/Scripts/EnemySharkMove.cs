﻿using UnityEngine;
using System.Collections;

public class EnemySharkMove : BaseClassShip
{
    EnemyShark defaultshark = new EnemyShark(3, 2);
    GameObject player;
	Vector3 targetPos;

	// Use this for initialization
	void Start () 
    {
		player = GameObject.Find ("player");
		targetPos = player.transform.position - transform.position;
        Destroy(gameObject, 10);
    }
	
	// Update is called once per frame
	void Update () 
    {
		defaultshark.Move (gameObject, targetPos);
    }
}
