﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Exercise
{
    class ConsumableItem : Item
    {
        //CONSTRUCTOR
        public ConsumableItem() { }
        public ConsumableItem(int recover)
        {
            recoverHP = recover;
        }
        public override void Use()
        {
            Console.WriteLine("You have used a consumable item.");
        }
        //public override int Use(int[] stat)
        //{
        //    return 0;
        //}
        private int recoverHP;
    }
}
