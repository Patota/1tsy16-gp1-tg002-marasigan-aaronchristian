﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Exercise
{
    class DamagingItem : Item
    {
        public DamagingItem() { }
        public DamagingItem(int damageAmount)
        {
            damage = damageAmount;
        }
        public override void Use()
        {
            Console.WriteLine("You have used a damaging item.");
        }
        //public override int Use(int[] stat)
        //{
        //    return 0;
        //}
        private int damage;
    }
}
