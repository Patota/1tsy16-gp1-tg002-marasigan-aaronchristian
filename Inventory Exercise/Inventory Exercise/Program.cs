﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            Inventory stash = new Inventory();
            Item testConsumableItem = new ConsumableItem();
            Item testBuffingItem = new BuffingItem();
            Item testDamagingItem = new DamagingItem();

            testConsumableItem.Use();
            testBuffingItem.Use();
            testDamagingItem.Use();

            Console.ReadKey();
            Console.Clear();
          
            int K = 1;
            do
            {
                Console.WriteLine("Please select item to place inside inventory");
                Console.WriteLine("1)Consumable item, 2)Buffing item, 3)Damaging item");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1: stash.addItem(new ConsumableItem()); break;
                    case 2: stash.addItem(new BuffingItem()); break;
                    case 3: stash.addItem(new DamagingItem()); break;
                }
                Console.WriteLine("Would you like to add more? 1)Yes, 2)No");
                K = Convert.ToInt32(Console.ReadLine());
            } while (K == 1);

            stash.UseAll();
            Console.ReadKey();
        }
    }
}
