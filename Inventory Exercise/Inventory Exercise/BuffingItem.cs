﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Exercise
{
    class BuffingItem : Item
    {
        public BuffingItem() { }
        public BuffingItem(int increaseHPAmount, int increaseMDEFAmount, int increasePDEFAmount, int increaseSpeedAmount)
        {
            increaseMaxHP = increaseHPAmount;
            increaseMDEF = increaseMDEFAmount;
            increasePDEF = increasePDEFAmount;
            increaseSpeed = increaseSpeedAmount;
        }
        public override void Use()
        {
            Console.WriteLine("You have used a buffing item.");
        }
        //public override int Use(int[] stat)
        //{   
        //    return 0;
        //}
        private int increaseMaxHP;
        private int increaseMDEF;
        private int increasePDEF;
        private int increaseSpeed;
    }
}
