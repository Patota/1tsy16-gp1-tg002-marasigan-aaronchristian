﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : BasicShip
{
	int enemyHP = 2;

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "playerBullet")
        {
            enemyHP -= 1;
            Destroy(col.gameObject);
            if (enemyHP < 1)
            {
                Destroy(gameObject);
            }
        }
    }
}
