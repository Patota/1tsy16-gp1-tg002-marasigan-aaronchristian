﻿using UnityEngine;
using System.Collections;

public class PlayerCannon : PlayerShip
{
	PlayerShip player = new PlayerShip();
	public Rigidbody bullet;
	int currentIndex = 0;
	GameObject nearestEnemy;

	// Use this for initialization
	void Start ()
    {
		InvokeRepeating ("shoot", 2, 1);
	}
	
	// Update is called once per frame
	void Update ()
    {
		nearestEnemy = FindClosestEnemy ();
	}

	GameObject FindClosestEnemy()
    {
		GameObject[] gos;
		gos = GameObject.FindGameObjectsWithTag("enemyShip");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;
		foreach (GameObject go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
		}
		return closest;
	}

	void shoot(){
		Rigidbody clonedBullet;
		//clone.velocity = transform.TransformDirection(Vector3.forward * 10);
		clonedBullet = Instantiate (bullet, transform.position, transform.rotation)as Rigidbody;
		Vector3 dir = nearestEnemy.transform.position - transform.position;
		clonedBullet.velocity += 100 * dir * Time.deltaTime;
}
}
