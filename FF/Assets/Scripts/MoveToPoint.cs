﻿using UnityEngine;
using System.Collections;

public class MoveToPoint : MonoBehaviour
{
    public class camera1
    {
        public float camSpeed;
        public camera1(int spd)
        {
            camSpeed = spd;
        }
    }

	public camera1 myCamera = new camera1(4);

	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
		Vector3 targetPos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 5));
		transform.position = Vector3.MoveTowards (transform.position, targetPos, myCamera.camSpeed * Time.deltaTime);
	}
}
