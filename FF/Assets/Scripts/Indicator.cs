﻿using UnityEngine;
using System.Collections;

public class Indicator : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
		Cursor.visible = false;
		Vector3 targetPos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 5));
		transform.position = targetPos;
	}
}
