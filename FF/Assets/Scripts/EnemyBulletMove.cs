﻿using UnityEngine;
using System.Collections;

public class EnemyBulletMove : BaseClassBullet
{
    GameObject player;
	Vector3 targetPos;
	Vector3 dir;
	basicBullet bullet = new basicBullet();

	// Use this for initialization
	void Start ()
    {
		player = GameObject.FindGameObjectWithTag ("player");
		targetPos = player.transform.position;
		dir = player.transform.position - transform.position;
		//transform.LookAt (player.transform.position);
	}

	// Update is called once per frame
	void Update ()
    {
		bullet.bulletMove (dir, 2, gameObject);
		if (Vector2.Distance(transform.position, player.transform.position) > 25.0f)
			Destroy (gameObject);	
	}
}
