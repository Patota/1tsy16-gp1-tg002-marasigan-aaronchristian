﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : PlayerShip
{
	public PlayerShip player = new PlayerShip(5, 4, 5);
	// Use this for initialization
	void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    { 
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "bullet" || col.gameObject.tag == "enemyShark")
		{
			player.PlayerHealth -= 1;
			Destroy(col.gameObject);

			if(player.PlayerHealth < 1)
			{
			Destroy(gameObject);
			}
		}
	}
}
