﻿using UnityEngine;
using System.Collections;


    public class EnemyShip : BasicShip
    {
        public EnemyShip(int hp, int spd, int fr)
        {
            MoveSpeed = spd;
            FireRate = fr;
            PlayerHealth = hp;
        }
        public EnemyShip()
        { }
    }

