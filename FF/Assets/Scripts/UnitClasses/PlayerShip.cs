﻿using UnityEngine;
using System.Collections;

public class PlayerShip : BasicShip
    {
        public PlayerShip(int hp, int spd, int fr)
        {
            MoveSpeed = spd;
            FireRate = fr;
            PlayerHealth = hp;
        }

        public PlayerShip()
        {
        }
    }

