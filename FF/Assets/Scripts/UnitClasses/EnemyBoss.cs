﻿using UnityEngine;
using System.Collections;


   public class EnemyBoss : BasicShip
    {
        public EnemyBoss(int hp, int spd, int fr)
        {
            MoveSpeed = spd;
            FireRate = fr;
            PlayerHealth = hp;
        }
    }

