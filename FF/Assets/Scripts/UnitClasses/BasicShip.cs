﻿using UnityEngine;
using System.Collections;

public class BasicShip : MonoBehaviour

{
    public int PlayerHealth;
	protected int MoveSpeed;
	public int FireRate;

	public void ShootCannon(Rigidbody bullet,GameObject thisObject)
    {
		Transform clone;
		clone = Instantiate(bullet, thisObject.transform.position, thisObject.transform.rotation) as Transform;
	}

	public void Move(GameObject thisShip, Vector3 targetPos)
		{
			thisShip.transform.position += 0.3f * targetPos * Time.deltaTime;
		}
}




