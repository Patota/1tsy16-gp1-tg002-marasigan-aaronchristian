﻿using UnityEngine;
using System.Collections;

public class EnemySharkMove : BasicShip
{
	GameObject player;
	Vector3 targetPos;
	public EnemyShark defaultShark = new EnemyShark(1, 4);

	// Use this for initialization
	void Start ()
    {
		player = GameObject.Find ("player");
		targetPos = player.transform.position - transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
		defaultShark.Move (gameObject, targetPos);
		if (transform.position == targetPos )
			Destroy (gameObject);
	}
}
