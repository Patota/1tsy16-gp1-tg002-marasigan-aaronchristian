﻿using UnityEngine;
using System.Collections;

public class EnemyShipMove : EnemyShip
{
	GameObject player;
	static int speed = 3;
	public EnemyShip defaultShip =  new EnemyShip(1, 3, 1);
	Vector3 targetPos;
	
	// Use this for initialization
	void Start ()
    {
		player = GameObject.FindGameObjectWithTag("player");	
	}

    // Update is called once per frame
    void Update()
    {
            targetPos = player.transform.position - transform.position;
            //Debug.Log (Vector3.Distance (transform.position, player.transform.position));
            if (Vector2.Distance(transform.position, player.transform.position) > 6)
            {
                defaultShip.Move(gameObject, targetPos);
            }
            if (Vector2.Distance(transform.position, player.transform.position) < 2)
            {
                defaultShip.Move(gameObject, targetPos);
            }
	}
}
