﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecettearShop
{
    static class NPCDatabase
    {
        public static NPC GetNpc(int id)
        {
            if (id == 1) return new NPC("Walnut Bread");
            else if (id == 2) return new NPC("Shortsword");
            else if (id == 3) return new NPC("Longsword");
            else if (id == 4) return new NPC("Armor");
            else if (id == 5) return new NPC("Shortcake");
            else if (id == 6) return new NPC("Candy");
            else return null;
        }
    }
}
