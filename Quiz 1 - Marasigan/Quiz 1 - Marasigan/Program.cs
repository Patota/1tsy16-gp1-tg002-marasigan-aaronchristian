﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz_1___Marasigan
{
    class Program
    {
        static Random random = new Random();
        static int GetNumSpies(int totalPlayers)
        {
            // 5-6 players = 2 spies
            // 7-8 players = 3 spies
            // 9-10 players = 4 spies
            if (totalPlayers >= 5 && totalPlayers <= 6)
                return 2;
            else if (totalPlayers >= 7 && totalPlayers <= 8)
                return 3;
            else if (totalPlayers >= 9 && totalPlayers <= 10)
                return 4;
            else
                return 0;     
        }

        static int EvaluateRound(string[] players)
        {
            int z = 0, y = 0;
            // Return 0 if resistance wins
            // Return 1 if spies win
            for (int x = 0; x < players.Length; x++)
            {
                if (players[x] == "Player")
                    z += 1;
                else if (players[x] == "Spy")
                    y += 1;
            }
            if (z > y)
                return 0;
            else
                return 1;
        }


        static void PrintPlayers(string[] players)
        {
            for (int x = 0; x < players.Length; x++)
            {
                Console.Write((x + 1) + ".");
                Console.WriteLine(players[x]);
            }
        }

        static void AssignRoles(string[] players)
        {
            // Must be dependent on the number of spies
            // Must always reach max # of spies. e.g. if there are 5 players there are always 2 spies
            // Must not exceed the # of spies for that set 
            // e.g if there are 7 players, there are always 3 spies, nothing less nothing more
            int SpyCount = GetNumSpies(players.Length);
            for (int x = 0; x < players.Length; x++)
            {
                players[x] = "Player";
            }
            while (SpyCount > 0)
            {
                int k = random.Next(players.Length);
                
                if (players[k] != "Spy")
                {
                    players[k] = "Spy";
                    SpyCount--;
                }
            }
            PrintPlayers(players);
        }

        static int GetPlayersForMission(int totalPlayers)
        {
            // 5-6 players = 3 per mission
            // 7-8 players = 4 per mission
            // 9-10 players = 5 per mission
            if (totalPlayers <= 6)
                return 3;
            else if (totalPlayers >= 7 || totalPlayers <= 8)
                return 4;
            else if (totalPlayers >= 9 || totalPlayers <= 10)
                return 5;
            else 
                return 0;
        }

        static bool ValidateIndex(int[] missionPlayersIndex, int randomIndex)
        {
            for (int j = 0; j < missionPlayersIndex.Length; j++)
            {
                int presentIndex = missionPlayersIndex[j];
                if (presentIndex == randomIndex)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Resistance!");

            int playersNum = 0;
            /////////////////////
            // QUIZ ITEM: Loop until the player enters a valid number
            /////////////////////
            while (playersNum < 5 || playersNum > 10)
            {
                Console.Write("Enter number of players (5-10)");
                playersNum = Convert.ToInt32(Console.ReadLine());
            }
            // END LOOP

            // Make an array of players based on the number the player entered
            string[] players = new string[playersNum];
            // Assign roles to the players
            AssignRoles(players);
            Console.ReadKey();

            int resistanceScore = 0;
            int spiesScore = 0;
            int roundNum = 1;
            // Begin the missions
            while (resistanceScore < 3 && spiesScore < 3)
            {
                Console.Clear();
                Console.WriteLine("Mission " + roundNum.ToString());
                Console.WriteLine("Resistance: " + resistanceScore.ToString());
                Console.WriteLine("Spies: " + spiesScore.ToString());

                // Select random players based on the players array
                int missionPlayerCount = GetPlayersForMission(playersNum);
                List<int> missionPlayersIndex = new List<int>();
                string[] missionPlayers = new string[missionPlayerCount];
                for (int i = 0; i < missionPlayerCount; i++)
                {
                    int randomIndex = 0;
                    bool alreadyTaken = true;
                    while (alreadyTaken)
                    {
                        randomIndex = random.Next(0, players.Length);
                        alreadyTaken = ValidateIndex(missionPlayersIndex.ToArray(), randomIndex);
                    }
                    missionPlayersIndex.Add(randomIndex);
                    missionPlayers[i] = players[randomIndex];
                }
                // END select random players

                // Play the mission
                int result = EvaluateRound(missionPlayers);
                // Add the result score
                if (result == 0)
                {
                    resistanceScore++;
                }
                else
                {
                    spiesScore++;
                }
                roundNum++;
            }
            // END missions

            if (resistanceScore >= 3)
            {
                Console.WriteLine("Resistance Wins!");
            }
            else if (spiesScore >= 3)
            {
                Console.WriteLine("Spies Wins!");
            }
            Console.ReadKey();

        }
    }
}

