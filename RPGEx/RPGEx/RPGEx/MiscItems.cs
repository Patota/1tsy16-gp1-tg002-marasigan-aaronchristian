﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    class MiscItems
    {
        public MiscItems(string name, int price)
        {
            Name = name;
            Price = price;
        }

        public string Name;
        public int Price;
    }
    
}
