﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    class SampleStore
    {
        public SampleStore(string name, int xlocation, int ylocation)
        {
            Name = name;
            xLocation = xlocation;
            yLocation = ylocation;
            itemSale.Add(new MiscItems("potion", 5));
            itemSale.Add(new MiscItems("French Fries", 30));
            itemSale.Add(new MiscItems("Watermelon", 3));
            itemSale.Add(new MiscItems("Bottled Water", 6));
            armorSale.Add(new Armor("Plate Mail", 200, 35));
            armorSale.Add(new Armor("Mitril helmet", 67, 12));
            armorSale.Add(new Armor("Bronze boots", 38, 6));
        }

        public void DisplayItems()
        {
            Console.WriteLine("choose an item type to display");
            int choice = Convert.ToInt32(Console.ReadLine());

            if (choice == 1)
            {
                DisplayArmor();
            }
            if (choice == 2)
            {
                DisplayItem();
            }
        }
        public void DisplayArmor()
        {
            for (int x = 0; x < armorSale.Count; x++)
            {
                Console.WriteLine(armorSale[x].Name+"def: "+ armorSale[x].Defense+"price: "+ armorSale[x].Price);
            }
        }
        public void DisplayItem()
        {
            for (int x = 0; x < armorSale.Count; x++)
            {
                Console.WriteLine(itemSale[x].Name + "price: " + itemSale[x].Price);
            }
        }
        public void OpenStore(Player mainPlayer)
        {
            Console.WriteLine("Greetings " + mainPlayer + "!");
            Console.WriteLine("Welcome to the store :D");
            Console.WriteLine("What do you need?");
            DisplayItems();

            Console.ReadKey();
        }
        public List<MiscItems> itemSale = new List<MiscItems>();
        public List<Armor> armorSale = new List<Armor>();
        public string Name;
        public int xLocation;
        public int yLocation;
    }
    
}
