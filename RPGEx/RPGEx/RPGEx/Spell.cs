﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Spell
    {
        public string Name;
        public Range DamageRange;
        public int MagicPointsRequired;

        //constructor
        public Spell(string spellName, int damagerangeLow, int damagerangeHigh, int magicPointsNeeded)
        {
            Name = spellName;
            DamageRange.Low = damagerangeLow;
            DamageRange.High = damagerangeHigh;
            MagicPointsRequired = magicPointsNeeded;
        }

        public int damageToDeal(int currentMagicPoints)
        {

            this.Name = "Magic Missile";
            this.DamageRange.Low = 10;
            this.DamageRange.High = 20;
            MagicPointsRequired = 15;

            if (MagicPointsRequired <= magicPoints)
                return RandomHelper.Random(this.DamageRange.Low, this.DamageRange.High);

            else
            {
                Console.WriteLine("You do not have enough magic points!");
                return 0;
            }
        }

        public int FireBall(int magicPoints)
        {
            this.Name = "Fire Ball";
            this.DamageRange.Low = 22;
            this.DamageRange.High = 23;
            MagicPointsRequired = 20;

            if (MagicPointsRequired <= magicPoints)
                return RandomHelper.Random(this.DamageRange.Low, this.DamageRange.High);

            else
            {
                Console.WriteLine("You do not have enough magic points!");
                return 0;
            }
        }

        public int ArcaneBlast(int magicPoints)
        {
            this.Name = "Arcane Blast";
            this.DamageRange.Low = 5;
            this.DamageRange.High = 5;
            MagicPointsRequired = 4;

            if (MagicPointsRequired <= magicPoints)

            if (MagicPointsRequired <= currentMagicPoints)
                return RandomHelper.Random(this.DamageRange.Low, this.DamageRange.High);

            else
            {
                Console.WriteLine("You do not have enough magic points!");
                return 0;
            }
        }
    }
}

        
            
       



    

