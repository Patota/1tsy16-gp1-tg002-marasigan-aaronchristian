﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Map gameMap = new Map();
            SampleStore store = new SampleStore("Store", 2, 2);
            Player mainPlayer = new Player();
            mainPlayer.CreateClass();

            // Begin adventure
            bool done = false;
            while (!done)
            {
                // Each loop cycle we output the player position
                // and a selection menu
                gameMap.PrintPlayerPos();

                int selection = 1;
                Console.Write("1) Move, 2) Rest, 3) View Stats, 4) Quit:");
                selection = Convert.ToInt32(Console.ReadLine());
                Monster monster = null;

                switch (selection)
                {
                    case 1:
                        gameMap.MovePlayer();

                        // Check for a random encounter. This function
                        // returns a null pointer if no monster are
                        // encoutered.
                        if (gameMap.getPlayerPosX() == store.xLocation && gameMap.getPlayerPosY() == store.yLocation)
                        {
                            store.OpenStore(mainPlayer);
                        }

                        monster = gameMap.CheckRandomEncounter();

                        if (monster != null)
                        {
                            while (true)
                            {
                                // Display hitpoints
                                mainPlayer.DisplayHitPoints();
                                monster.DisplayHitPoints();
                                Console.WriteLine("1)Attack 2)Cast Spell");
                                int choice = Convert.ToInt32(Console.Read());
                                bool runAway;

                                if (choice == 1)
                                {
                                    runAway = mainPlayer.Attack(monster);
                                    if (runAway)
                                        break;
                                }
                                if (choice == 2)
                                {
                                    runAway = mainPlayer.castSpell(monster);
                                    if (runAway)
                                        break;
                                }

                                if (monster.isDead)
                                {
                                    mainPlayer.Victory(monster.ExpReward,monster.GoldReward);
                                    mainPlayer.LevelUp();
                                    break;
                                }

                                monster.Attack(mainPlayer);

                                if (mainPlayer.isDead)
                                {
                                    mainPlayer.GameOver();
                                    done = true;
                                    break;
                                }
                            }
                        }
                        break;
                    case 2:
                        monster = gameMap.CheckRandomEncounter();
                        mainPlayer.Rest(mainPlayer, monster, done);
                        break;
                    case 3:
                        mainPlayer.ViewStats();
                        break;
                    case 4:
                        done = true;
                        break;
                }
            }
        }
    }
}
