﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Weapon
    {
        public Weapon(string name, int lowDamage, int highDamage)
        {
            Name = name;
            DamageRange.Low = lowDamage;
            DamageRange.High = highDamage;
        }

        public string Name { get; set; }
        public Range DamageRange;
    }
}
