﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Player
    {
        public string Name { get; private set; }

        // Constructor
        public Player()
        {
            Name = "Default";
            raceName = "default";
            className = "Default";
            accuracy = 0;
            hitPoints = 0;
            maxHitPoints = 0;
            magicPoints = 0;
            maxMagicPoints = 0;
            expPoints = 0;
            gold = 0;
            nextLevelExp = 0;
            level = 0;
            Armor = 0;
            weapon = new Weapon("Default Weapon Name", 0, 0);           
        }

        public void CreateClass()
        {
            Console.WriteLine("CHARACTER CLASS GENERATION");
            Console.WriteLine("==========================");

            // Input character's name.
            Console.WriteLine("Enter your character's name: ");
            Name = Console.ReadLine();

            Console.WriteLine("Please select a character class number...");
            Console.WriteLine("1)Fighter 2)Wizard 3)Cleric 4)Thief : ");

            int characterNum = 1;

            characterNum = Convert.ToInt32(Console.ReadLine());

            switch (characterNum)
            {
                case 1: //Fighter
                    className = "Fighter";
                    accuracy = 10;
                    hitPoints = 20;
                    maxHitPoints = 20;
                    magicPoints = 10;
                    maxMagicPoints = 10;
                    expPoints = 0;
                    gold = 10;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 4;
                    weapon = new Weapon("Long Sword", 1, 8);
                    Spell.Add(new Spell("Hammer Throw", 7, 10, 5));
                    Spell.Add(new Spell("Forehead Smash", 20, 300, 10));
                    break;
                case 2: //Wizard
                    className = "Wizard";
                    accuracy = 5;
                    hitPoints = 10;
                    maxHitPoints = 10;
                    magicPoints = 20;
                    maxMagicPoints = 20;
                    expPoints = 0;
                    gold = 10;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 1;
                    weapon = new Weapon("Staff", 1, 4);
                    Spell.Add(new Spell("Magic Missile", 15, 20, 10));
                    Spell.Add(new Spell("Dark Energy Bolt", 20,25, 15));
                    Spell.Add(new Spell("Fire Blast", 6, 33, 18));
                    break;
                case 3:
                    className = "Cleric";
                    accuracy = 8;
                    hitPoints = 15;
                    maxHitPoints = 15;
                    magicPoints = 13;
                    maxMagicPoints = 13;
                    expPoints = 0;
                    gold = 10;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 3;
                    weapon = new Weapon("Flail", 1, 6);
                    Spell.Add(new Spell("Holy Smite", 17, 18, 10));
                    Spell.Add(new Spell("Light of Heaven", 1000, 1001, 13));
                    Spell.Add(new Spell("Soul Rip", 7, 12, 6));
                    break;
                default: //
                    className = "Thief";
                    accuracy = 7;
                    hitPoints = 12;
                    maxHitPoints = 12;
                    magicPoints = 8;
                    maxMagicPoints = 8;
                    expPoints = 0;
                    gold = 10;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 2;
                    weapon = new Weapon("Dagger", 1, 6);
                    Spell.Add(new Spell("Dagger Toss", 2, 5, 2));
                    Spell.Add(new Spell("Flying Fist", 998, 999, 8));
                    break;
            }
            ChooseRace();
        }
        public void ChooseRace()
        {
            Console.WriteLine("CHARACTER RACE SELECTION");
            Console.WriteLine("==========================");

            Console.WriteLine("Please select a character race number...");
            Console.WriteLine("1)Dwarf 2)Halfling 3)Human 4)Dinosaur : ");

            int characterNum = 1;

            characterNum = Convert.ToInt32(Console.ReadLine());

            switch (characterNum)
            {
                case 1: //Dwarf
                    raceName = "Dwarf";
                    accuracy += -5;
                    hitPoints += 3;
                    maxHitPoints += 3;
                    expPoints += 0;
                    gold += 0;
                    nextLevelExp += 0;
                    level += 0;
                    Armor += 5;
                    break;
                case 2: //Halfling
                    raceName = "Halfling";
                    accuracy += -2;
                    hitPoints += -3;
                    maxHitPoints += -3;
                    expPoints += 0;
                    gold += 0;
                    nextLevelExp += -200;
                    level += 3;
                    Armor += -9;
                    break;
                case 3: //Human
                    raceName = "Human";
                    accuracy += -1;
                    hitPoints += 6;
                    maxHitPoints += 6;
                    expPoints += 0;
                    gold += 25;
                    nextLevelExp += 1100;
                    level += 0;
                    Armor += 3;
                    break;
                default: //Dinosaur
                    raceName = "Dinosaur";
                    accuracy += -30;
                    hitPoints += 350;
                    maxHitPoints += 350;
                    expPoints += 0;
                    gold += -50;
                    nextLevelExp += -900;
                    level += 20;
                    Armor += 45;
                    break;
            }

        }

        public bool isDead { get { return hitPoints <= 0; } }
        public int Armor { get; private set;}

        public bool castSpell(Monster monsterTarget)
        {
            Console.Write("1) Select spell to cast, 2) Run");
            int select = Convert.ToInt32(Console.Read());
            switch (select)
            {///////////////////////////////TO FIX
                case 1:
                    displaySpells();
                    int temp = Convert.ToInt32(Console.Read());
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = Spell[temp].damageToDeal(this.magicPoints);
                        int totalDamage = damage - monsterTarget.Armor;
                        if (totalDamage <= 0)
                        {
                            Console.WriteLine("Your attack failed to penetrate the armor");
                        } else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss!");
                    }
                    break;

                case 2:
                    Console.WriteLine("Choose A Spell to cast on target:");
                    Console.WriteLine("1)Magic Missile, 2)Fire Ball, 3)Arcane Blast");
                    int choice = Convert.ToInt32(Console.Read());
                    Spell s1 = new Spell();
                        
                   /////////////////////////////// will fix
                   if(choice == 1)


                    Console.WriteLine("You Cast a Spell on " + monsterTarget.Name + " with a " + weapon.Name);

                default:
                    // 25% chance of being able to run
                    int roll = RandomHelper.Random(1, 4);
                    if (roll == 1)
                    {
                        Console.WriteLine("You run away!");
                        return true; //<-- Return out of the function
                    }
                    else
                    {
                        Console.WriteLine("You could not escape!");
                    }
                    break;
            }

            return false;
        }
        public void displaySpells()
        {
            for (int x = 0; x < Spell.Count; x++)
            {
                Console.Write(x + ")");
                Console.WriteLine(Spell[x].Name);
            }
        }
        public bool Attack(Monster monsterTarget)
        {
            
            Console.Write("1) Attack, 2) Run");
            int select = Convert.ToInt32(Console.Read());
            switch (select)
            {
                case 1:
                    Console.WriteLine("You attack an " + monsterTarget.Name + " with a " + weapon.Name);
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = RandomHelper.Random(weapon.DamageRange);
                        int totalDamage = damage - monsterTarget.Armor;
                        if (totalDamage <= 0)
                        {
                            Console.WriteLine("Your attack failed to penetrate the armor");
                        } else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss!");
                    }
                    break;

                default:
                    // 25% chance of being able to run
                    int roll = RandomHelper.Random(1, 4);
                    if (roll == 1)
                    {
                        Console.WriteLine("You run away!");
                        return true; //<-- Return out of the function
                    }
                    else
                    {
                        Console.WriteLine("You could not escape!");
                    }
                    break;
            }

            return false;
        }

        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints < 0)
            {
                hitPoints = 0;
            }
        }

        public void LevelUp()
        {
            if (expPoints >= nextLevelExp)
            {
                Console.WriteLine("You gained a level!");

                // Increment level.
                level++;

                // Set experience points required for next level
                nextLevelExp = level * level * 1000;

                // Increase stats randomly
                    if (className == "fighter")
                {
                    accuracy += RandomHelper.Random(1, 2);
                    maxHitPoints += RandomHelper.Random(7, 10);
                    Armor += RandomHelper.Random(3, 4);
                    maxMagicPoints += RandomHelper.Random(2, 4);
                }
                if (className == "wizard")
                {
                    accuracy += RandomHelper.Random(2, 4);
                    maxHitPoints += RandomHelper.Random(2, 4);
                    Armor += RandomHelper.Random(1, 2);
                    maxMagicPoints += RandomHelper.Random(8, 10);
                }
                if (className == "cleric")
                {
                    accuracy += RandomHelper.Random(1, 2);
                    maxHitPoints += RandomHelper.Random(3, 5);
                    Armor += RandomHelper.Random(1, 2);
                    maxMagicPoints += RandomHelper.Random(6, 9);
                }
                if (className == "thief")
                {
                    accuracy += RandomHelper.Random(6, 10);
                    maxHitPoints += RandomHelper.Random(4, 6);
                    Armor += RandomHelper.Random(5, 7);
                    maxMagicPoints += RandomHelper.Random(3, 5);
                }

                // Give the player full hitpoints when they level up.
                hitPoints = maxHitPoints;
            }
        }

        public void Rest(Player mainPlayer, Monster monster, bool done)
        {
            Console.WriteLine("Resting...");

            hitPoints = maxHitPoints;

            // TODO: Modify the function so that random enemy enounters
            // are possible when resting
            if (monster != null)
            {
                while (true)
                {
                    // Display hitpoints
                    DisplayHitPoints();
                    monster.DisplayHitPoints();
                    Console.WriteLine();

                    bool runAway = Attack(monster);

                    if (runAway)
                        break;

                    if (monster.isDead)
                    {
                        Victory(monster.ExpReward, monster.GoldReward);
                        LevelUp();
                        break;
                    }

                    monster.Attack(mainPlayer);

                    if (isDead)
                    {
                        mainPlayer.GameOver();
                        done = true;
                        break;
                    }
                }
            }

        }
        public void ViewStats()
        {
            Console.WriteLine("PLAYER STATS");
            Console.WriteLine("============");
            Console.WriteLine();

            Console.WriteLine("Name             = " + Name);
            Console.WriteLine("Class            = " + className);
            Console.WriteLine("Accuracy         = " + accuracy.ToString());
            Console.WriteLine("Hitpoints        = " + hitPoints.ToString());
            Console.WriteLine("MaxHitpoints     = " + maxHitPoints.ToString());
            Console.WriteLine("MagicPoints      = " + magicPoints.ToString());
            Console.WriteLine("MaxMagicPoints   = " + maxMagicPoints.ToString());
            Console.WriteLine("XP               = " + expPoints.ToString());
            Console.WriteLine("XP to next level = " + nextLevelExp.ToString());
            Console.WriteLine("Level            = " + level.ToString());
            Console.WriteLine("Armor            = " + Armor.ToString());
            Console.WriteLine("Weapon Name      = " + weapon.Name);
            Console.WriteLine("Weapon Damage    = " + weapon.DamageRange.Low.ToString() + "-" + weapon.DamageRange.High.ToString());
            Console.WriteLine("Gold             = " + gold.ToString());
            Console.WriteLine();
            Console.WriteLine("END PLAYER STATS");
            Console.WriteLine("================");
            Console.WriteLine();
        }
        
        public void Victory(int xp, int goldReward)
        {
            Console.WriteLine("You won the battle!");
            Console.WriteLine("You win " + xp.ToString() + " experience points!");
            expPoints += xp;
            gold += RandomHelper.Random(goldReward,goldReward+4);
        }

        public void GameOver()
        {
            Console.WriteLine("You died in battle...");
            Console.WriteLine();
            Console.WriteLine("==========================");
            Console.WriteLine("GAME OVER!");
            Console.WriteLine("==========================");
            Console.ReadLine();
        }

        public void DisplayHitPoints()
        {
            Console.WriteLine(Name + "'s hitpoints = " + hitPoints.ToString());
        }

        private List<Spell> Spell = new List<Spell>();
        private string className;
        private string raceName;
        private int hitPoints;
        private int maxHitPoints;
        private int expPoints;
        private int nextLevelExp;
        private int level;
        private int accuracy;
        private Weapon weapon;
        private int gold;
        private int magicPoints;
        private int maxMagicPoints;
    }
}
