﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    class Armor
    {
        public Armor(string name, int price, int defense)
        {
            Name = name;
            Price = price;
            Defense = defense;
        }

        public string Name;
        public int Price;
        public int Defense;
    }
}
