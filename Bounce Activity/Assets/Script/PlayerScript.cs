﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
    

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            this.transform.Translate(Vector2.right * 5 * Time.deltaTime);
        if (Input.GetKey(KeyCode.LeftArrow))
            this.transform.Translate(Vector2.right * -5 * Time.deltaTime);
        if (Input.GetKey(KeyCode.UpArrow))
            this.transform.Translate(Vector2.up * 5 * Time.deltaTime);
        if (Input.GetKey(KeyCode.DownArrow))
            this.transform.Translate(Vector2.down * 5 * Time.deltaTime);

        if(this.transform.position.y < GameObject.FindGameObjectWithTag("Ground").transform.position.y)
            this.transform.position = new Vector3(1.11f, 1.11f, 0);


    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.name == "Cube")
        {
            Debug.Log("collided with box");
            this.transform.position = new Vector3(1.11f, 1.11f, 0);
        }

       else if (col.transform.name == "Sphere")
        {
            Debug.Log("collided with circle");
            this.GetComponent<Rigidbody2D>().gravityScale = 1;
        }

        else if (col.transform.name == "Capsule")
        {
            Debug.Log("collided with capsule");
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
        }
    }
}
