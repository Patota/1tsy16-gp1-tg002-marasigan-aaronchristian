﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Attack_Class
{
    class Unit
    {
       public int HP;
       public int Damage;

    }
    class Player : Unit
    {
        public Player()
        {
            this.HP = 100;
            this.Damage = 15;
        }
        public void ViewStats()
        {
            Console.WriteLine("Player has "+Convert.ToString(this.HP)+" HP");
            Console.WriteLine("PLayer has "+Convert.ToString(this.Damage)+" Damage");
        }
        public int Attack()
        {
            return this.Damage;
        }

        public void TakeDamage(int DamageSource)
        {
            this.HP -= DamageSource;
        }
        public string CheckState()
        {
            if (this.HP <= 0)
                return "Player is dead";
            else
                return "Player has "+Convert.ToString(this.HP)+" HP remaining";
        }
    }
    class Monster : Unit
    {
        public Monster()
        {
            this.HP = 100;
            this.Damage = 10;
        }
        public void ViewStats()
        {
            Console.WriteLine("Monster has " + Convert.ToString(this.HP) + " HP");
            Console.WriteLine("Monster has " + Convert.ToString(this.Damage) + " Damage");
        }
        public int Attack()
        {
            return this.Damage;
        }
        public void TakeDamage(int DamageSource)
        {
            this.HP -= DamageSource;
        }
        public string CheckState()
        {
            if (this.HP <= 0)
                return "Monster is dead";
            else
                return "Monster has "+Convert.ToString(this.HP)+" HP remaining";
        }
    }
}
