﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample_Attack_Class
{
    class Program
    {
        static void Main(string[] args)
        {
            Player P1 = new Player();
            Monster M1 = new Monster();

            P1.ViewStats();
            M1.ViewStats();

            Console.WriteLine("Monster attacks player");
            P1.TakeDamage(M1.Damage);
            Console.WriteLine(P1.CheckState());

            Console.WriteLine("Player attacks player");
            M1.TakeDamage(P1.Damage);
            Console.WriteLine(M1.CheckState());

            Console.ReadKey();
        }
    }
}
